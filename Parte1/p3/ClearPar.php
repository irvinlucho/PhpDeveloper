<?php
/**
 * Created by PhpStorm.
 * User: irvinstone
 * Date: 14/01/2017
 * Time: 01:31 AM
 */
Class ClearPar {
    public function build($string){
        $pila = new SplStack();//instanciamos la pila
        $string = str_split($string);//convertimos la cadena en array
        $salida = "";//inicializamos la salida

        foreach ($string as $c){//por cada caracter

            if($c == "("){
                $pila->push($c);//si es el parentesis de apertura enpilamos en la pila
            }elseif($c == ")"){// si es el parentesis de cierre
               if(!$pila->isEmpty()){// y la pila no esta vacia
                   $ultimo = $pila->pop();//depilamos
                   if($ultimo == "(")//en caso el ultimo  fuera el de apertura
                       $salida = $salida."()";// tenemos im parentesis completo
               }
            }
        }
        return $salida;
    }
}

$clase = new ClearPar();
echo $clase->build("()())()");echo "\n";
echo $clase->build("()(()");echo "\n";
echo $clase->build(")(");echo "\n";
echo $clase->build("((()");


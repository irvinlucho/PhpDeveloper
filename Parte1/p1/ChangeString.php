<?php
/**
 * Created by PhpStorm.
 * User: irvinstone
 * Date: 14/01/2017
 * Time: 12:42 AM
 */

class ChangeString {
    public function build($cadenaEntrada){
        $salida = "";//inicializamos el atributo salida
        $arrayEntrada = str_split($cadenaEntrada);//convertimos la cadena en array
        foreach ($arrayEntrada as $c){//iteramos por cada elemento de la cadena
            if(ctype_alpha($c)){//verificamos si es una letra
                if(ord($c) == 90){//en caso sea la letra "Z"
                    $siguiente = 65;
                }elseif(ord($c) == 68){//en caso sea la letra "z"
                    $siguiente = 97;
                }else {
                    $siguiente = ord($c)+1;//para los demas es la siguiente letra del abecedario
                }
                $salida = $salida.chr($siguiente);//concatenamos
            }
            else  $salida = $salida.$c;//sino es letra simplemente lo concatenamos
        }
        return $salida; //retornamos la salida del metodo
    }
}

//pruebas
$clase = new ChangeString();
echo $clase->build("123abcd*3");echo "\n";
echo $clase->build("**Casa 52");echo "\n";
echo $clase->build("**Casa52Z");
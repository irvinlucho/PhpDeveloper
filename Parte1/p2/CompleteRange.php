<?php
/**
 * Created by PhpStorm.
 * User: irvinstone
 * Date: 14/01/2017
 * Time: 01:31 AM
 */
Class CompleteRange {
    public function build($coleccion){//coleccion de enteros positivos ordenados ascendentemente
        $inicio = $coleccion[0];//el primer elemento
        $fin = $coleccion[count($coleccion)-1];//ultimo elemento
        $salida = [];//inicializamos el array de salida
        for($i = $inicio;$i<=$fin;$i++){
            $salida[] = $i; //desde el principio completamos todos los elementos puesto que son enteros positivos
        }
        return $salida;//retornamos la salida
    }
}

$clase = new CompleteRange();
//print_r($clase->build([4,6,7,10]));
print_r($clase->build([1,2,4,5]));
print_r($clase->build([2,4,9]));
print_r($clase->build([55,58,60]));
# Solucion Test



## Requisitos


* php >= 5.5
* composer
* Slim >= 3.1

para correr la aplicacion ubicarse en la carpeta Aplicativo y correr el siguiente comando.
el cual habilitara el servidor local http://localhost:8080/

	composer start

para probar el web service XML usar postman o curl enviando los datos de entrada en formato json para obtener
la lista de empleados con el salario correspondiente a ese rango en formato xml.

	{"desde":1190.84,"hasta":1193}

Muchas Gracias

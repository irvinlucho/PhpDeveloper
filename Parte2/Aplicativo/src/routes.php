<?php
// Routes
$app->get('/', function ($request, $response, $args) {

    $file = __DIR__ . "../employees.json";//ubicacion del archivo
    $data = $request->getParams();//obtenemos los datos del request
    $json = json_decode(file_get_contents($file), true);//leemos y parseamos el archivo json

    if(isset($data['email']) && $data['email'] != ""){//si el campo email no esta vacio
        $respuesta = array_filter(//filtramos todos los elementos que tengan el campo email igual al brindado
            $json,
            function ($e) use (&$data) {
                return $e["email"] == $data['email'];
            }
        );
    }else{
        $respuesta = $json;
    }
//retornamo la respuesta
    return $this->renderer->render($response, 'index.phtml', ["json"=>$respuesta]);

});
$app->get('/empleado/{id}', function ($request, $response, $args) {
   $data = $args['id'];
    $file = __DIR__ . "../employees.json";
    $json = json_decode(file_get_contents($file), true);
    $respuesta = array_filter(
        $json,
        function ($e) use (&$data) {
            return $e["id"] == $data;
        }
    );
    return $this->renderer->render($response, 'detalles.phtml', ["json"=>$respuesta]);
});

/*
 * Servicio web Que responde en formato xml
 */
$app->post('/empleados', function ($request, $response, $args) {
    $data = $request->getParams();
    $file = __DIR__ . "../employees.json";
    $json = json_decode(file_get_contents($file), true);

    $respuesta = array_filter(
        $json,
        function ($e) use (&$data) {
            $salario = doubleval(str_replace(",","",substr($e['salary'],1)));//convertimos en double para hacer las comparaciones
            if($salario >= $data['desde'] && $salario <= $data['hasta'])
                return $e;
        }
    );

    $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><employees></employees>");//generador de xml
    array_to_xml($respuesta,$xml);//methodo para convertir array a formato xml
    return $this->response->write($xml->asXML());//respondemos el xml
});


//codigo investigado de http://pastebin.com/pYuXQWee
function array_to_xml($student_info, &$xml_student_info) {
    foreach($student_info as $key => $value) {
        if(is_array($value)) {
            $key = is_numeric($key) ? "item$key" : $key;
            $subnode = $xml_student_info->addChild("$key");
            array_to_xml($value, $subnode);
        }
        else {
            $key = is_numeric($key) ? "item$key" : $key;
            $xml_student_info->addChild("$key","$value");
        }
    }
}
